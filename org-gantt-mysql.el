(emacsql-fix-vector-indentation)
;;|--------------------------------------------------------------
;;|Description : send planning to mysql db
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 14-35-2024 09:35:23
;;|--------------------------------------------------------------
(defun og-saveToSql ()
  "send planning to mysql db"
  (interactive)
  (let* ((db (emacsql-mysql "LPF"
              :user og-mysql-user
              :password og-mysql-password
              :host og-mysql-host
              :port "3306"))
         (dummy (message "after connect"))
         (id (nth 0 (nth 0 (emacsql db [:select id :from Planning :where (= project $s1)] og-title))))
         (dummy (message "after id"))
         (resources (mapconcat
                     'identity
                     (cl-loop for r in og-resources
                              for name = (nth 0 (car (emacsql db [:select name
                                                                  :from utilisateurs
                                                                  :where (= userId $r1)]
                                                              r
                                                              )
                                                     )
                                              )
                              collect (format "{\"id\":\"%s\",\"name\":\"%s\"}"
                                              r
                                              name
                                              )
                              )
                     ","
                     )
                    )
         (dummy (message "after resources"))
         (roles "")
         (json (og-toJson resources roles))
         )
    (message "%s" json)
    (if id
        (emacsql db [:update Planning :set (= Data $s1) :where (= id $s2) ] json id)
      (emacsql db [:insert-into Planning [project,Data,responsable] :values [$s1 $s2 0]] og-title json)
    )
    )
  )
;;|--------------------------------------------------------------
;;|Description : transform the planning structure to JSON for storage
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 15-12-2024 11:12:05
;;|--------------------------------------------------------------
(defun og-toJson (resources roles)
  "transform the planning structure to JSON for storage"
  (interactive)
  (let* ((json "{\"tasks\":[")
         (json (format "%s%s" json
                       (string-join (cl-loop for task in og-project collect (og-task2json task)) ",")
                                          )
               )
         (json (concat json (format "],\"resources\":[%s]" resources)))
         (json (concat json ",\"roles\":[]}"))
         )
    json
    )
  )
;;|--------------------------------------------------------------
;;|Description : task to json
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 15-00-2024 12:00:51
;;|--------------------------------------------------------------
(defun og-task2json (task)
  "task to json"
  (interactive)
  ;; (og-print-task task)
  (let* ((level (cond
                 ((string= "Title" (myTask-Id task)) 1)
                 (t (+ 2 (abs (myTask-Level task))))
                 )
                )
         (start (truncate (* 1000 (ts-unix (myTask-Start task)))))
         ;; (duration (max 1 (truncate (/ (ts-diff (myTask-End task) (myTask-Start task)) (* 24 3600)))))
         (duration (max 1 (og-findWorkdays (myTask-Start task) (myTask-End task))))
         ;; (dummy (message "id=%S D=%s" (myTask-Id task) duration))
         (id (format "%s_%s" og-title (myTask-Id task)))
         (progress (* 100 (myTask-Done task)))
         (status (cond ((= progress 100) "\"STATUS_DONE\"")
                       ((> progress 0) "\"STATUS_ACTIVE\"")
                       (t "\"STATUS_UNDEFINED\"")))
         (json (format "{\"id\":\"%s\","    id))
         (json (format "%s\"code\":\"%s\"," json  (myTask-Id task)))
         (json (format "%s\"name\":\"%s\"," json (myTask-Description task)))
         (json (if (string-match-p "^[[:blank:]]*$" (myTask-Content task))
                   json
                 (format "%s\"content\":%s," json (json-encode (myTask-Content task)))
                 )
               )
         (json (format "%s\"objective\":%s," json (json-encode (myTask-Objective task))))
         (json (format "%s\"level\":%s,"    json level))
         (json (format "%s\"start\":%s,"    json start))
         (json (format "%s\"duration\":%s,"  json duration))
         (json (format "%s\"status\":%s,"  json status))
         (json (format "%s\"responsable\":\"%s\","  json
                       (if (string= (myTask-Id task) "Title")
                           "hylkema"
                         (myTask-Resp task)
                         )
                       )
               )
         (json (format "%s\"avec\":\"%s\","  json (string-join (myTask-Avec task) ",")))
         (json (if (myTask-After task)
                   (format "%s\"depends\":\"%s\","  json (string-join
                                                      (cl-loop for task in (myTask-After task)
                                                               collect (myTask-Index task)
                                                               )
                                                      ",")
                           )
                 json 
                 )
               )
         (json (format "%s\"endIsMilestone\":\"%s\"," json (string= "KP" (myTask-Type task))))
         (json (format "%s\"progress\":%s"  json progress))
         (json (format "%s}"           json ))
         )
    json
    )
  )

;;|--------------------------------------------------------------
;;|Description : find the number of workdays between start and end dates
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 16-16-2024 13:16:05
;;|--------------------------------------------------------------
(defun og-findWorkdays (S E)
  "find the number of workdays between start and end dates"
  (interactive)
  (let* ((N 0))
    ;; (message "dbg: E=%s S=%s" (og-fmt-time E) (og-fmt-time S))
    (while (ts> E S)
      ;; (message "dbg: E=%s S=%s" (og-fmt-time E) (og-fmt-time S))
      ;; (message "# %s (%s)" (ts-day-of-week-num S) (< (ts-day-of-week-num S)))
      (setq S (ts-adjust 'day +1 S))
      (if (< 0 (ts-day-of-week-num S) 6) (setq N (+ N 1)))
      )
    N
    )
  )
(provide 'org-gantt-mysql)




